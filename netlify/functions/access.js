
const {
  NETLIFY_DEV = false,
  PIN,
  MARSHALL_CALLBOX_NUMBER, NINTH_CALLBOX_NUMBER,
} = process.env;

const error = {
  statusCode: 200,
  headers: {
    'Content-Type': 'text/xml; charset=utf-8',
  },
  body: `<?xml version="1.0" encoding="UTF-8"?>
<Response>
    <Say>There was a problem. Goodbye!</Say>
</Response>`
};

const failure = {
  statusCode: 200,
  headers: {
    'Content-Type': 'text/xml; charset=utf-8',
  },
  body: `<?xml version="1.0" encoding="UTF-8"?>
<Response>
    <Say>Incorrect PIN. Goodbye!</Say>
</Response>`
};

const gather = `<?xml version="1.0" encoding="UTF-8"?>
<Response>
    <Gather input="dtmf" numDigits="3">
        <Say>Please use the keypad to enter your pin.</Say>
    </Gather>
    <Say>We didn't receive any input. Goodbye!</Say>
</Response>`;

const access_granted = `<?xml version="1.0" encoding="UTF-8"?>
<Response>
    <Say>Thank you.  Access Granted.</Say>
    <Play digits="wwww999"></Play>
    <Say>Goodbye!</Say>
</Response>`;

exports.handler = async function(event, context) {
  const { httpMethod, queryStringParameters, body } = event;

  try {
    if (httpMethod === 'GET') {
      const { From } = queryStringParameters;
      console.log({From})
      if (From === MARSHALL_CALLBOX_NUMBER) {
        //return wrongDoor;
      }
      if (From === NINTH_CALLBOX_NUMBER) {
      }
      return {
        statusCode: 200,
        headers: {
          'Content-Type': 'text/xml; charset=utf-8',
        },
        body: gather,
      }
    } else if (httpMethod === 'POST') {
      //application/x-www-form-urlencoded
      const params = new URLSearchParams(event.body)
      const Digits = params.get('Digits')
      if (PIN === Digits) {
        return {
          statusCode: 200,
          headers: {
            'Content-Type': 'text/xml; charset=utf-8',
          },
          body: access_granted,
        }
      }
    }
  } catch (e) {
    console.error(e);
    return error;
  }
  return failure;
};
